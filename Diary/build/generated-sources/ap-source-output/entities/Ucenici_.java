package entities;

import entities.Odeljenja;
import entities.Roditelji;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-16T14:55:50")
@StaticMetamodel(Ucenici.class)
public class Ucenici_ { 

    public static volatile SingularAttribute<Ucenici, String> ime;
    public static volatile SingularAttribute<Ucenici, String> prezime;
    public static volatile SingularAttribute<Ucenici, Roditelji> roditeljiId;
    public static volatile SingularAttribute<Ucenici, Integer> id;
    public static volatile SingularAttribute<Ucenici, Odeljenja> odeljenjaId;

}