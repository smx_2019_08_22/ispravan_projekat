package entities;

import entities.Casovi;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-16T14:55:50")
@StaticMetamodel(Predmeti.class)
public class Predmeti_ { 

    public static volatile SingularAttribute<Predmeti, String> ime;
    public static volatile SingularAttribute<Predmeti, Casovi> casoviId;
    public static volatile SingularAttribute<Predmeti, Integer> id;

}