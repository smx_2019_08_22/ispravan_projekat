package entities;

import entities.Odeljenja;
import entities.Predmeti;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-16T14:55:50")
@StaticMetamodel(Casovi.class)
public class Casovi_ { 

    public static volatile ListAttribute<Casovi, Predmeti> predmetiList;
    public static volatile SingularAttribute<Casovi, String> broj;
    public static volatile SingularAttribute<Casovi, Integer> id;
    public static volatile ListAttribute<Casovi, Odeljenja> odeljenjaList;

}