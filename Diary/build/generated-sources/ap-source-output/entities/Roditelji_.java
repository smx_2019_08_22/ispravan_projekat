package entities;

import entities.Korisnici;
import entities.Poruke;
import entities.Ucenici;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-16T14:55:50")
@StaticMetamodel(Roditelji.class)
public class Roditelji_ { 

    public static volatile SingularAttribute<Roditelji, String> ime;
    public static volatile SingularAttribute<Roditelji, String> prezime;
    public static volatile ListAttribute<Roditelji, Ucenici> uceniciList;
    public static volatile SingularAttribute<Roditelji, Integer> id;
    public static volatile ListAttribute<Roditelji, Poruke> porukeList;
    public static volatile SingularAttribute<Roditelji, Korisnici> korisniciId;

}